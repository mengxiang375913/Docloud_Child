from models import *
from apps.common.errorcode import *


def get_filename_by_id(id):
    try:
        docs = Document.objects.get(id=id)
        return docs.storage
    except Exception as e:
        print e
        return Errorquery(str(e))
