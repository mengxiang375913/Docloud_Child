from __future__ import unicode_literals

from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r"^list$", index_view),
    url(r"^archivelist$", index_classes_view),
    url(r"^(?P<meta>\d+)/list$", index_view),
    url(r"^(?P<meta>\d+)/clslist$", index_classes_view),
    url(r"^(?P<meta>\d+)/list2$", uplade_view),
    url(r"^(?P<meta>\d+)/preview$", preview_view),
    url(r"^preview$", preview_view),
]
