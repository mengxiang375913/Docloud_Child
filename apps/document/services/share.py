# coding=utf-8
from common.errorcode import Errorquery


def add_share(params):
    """
    添加分享
    :param params:
    :return:
    """
    try:
        object = None
        return object
    except Exception as e:
        print e
        return Errorquery(str(e))


def del_share(params):
    """
    删除分享
    :param params:
    :return:
    """
    try:
        object = None
        return object
    except Exception as e:
        print e
        return Errorquery(str(e))


def info_share(params):
    """
    分享详情
    :param params:
    :return:
    """
    try:
        return object
    except Exception as e:
        print e
        return Errorquery(str(e))


def update_share(params):
    """
    更新分享
    :param params:
    :return:
    """
    try:
        object = None
        id = params["id"]
        del params["id"]
        return object
    except Exception as e:
        print e
        return Errorquery(str(e))


def list_share(params):
    """
    分享列表
    :param params:
    :return:
    """
    try:
        object = []
        return object
    except Exception as e:
        print e
        return Errorquery(str(e))
