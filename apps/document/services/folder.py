# coding=utf-8
from common.errorcode import Errorquery
from apps.document.models import *


def add_folder(params):
    """
    添加文件夹
    :param params:
    :return:
    """
    try:
        if params.get("org"):
            params["org"] = Orginazation.objects.get(id=params["org"])
        if Folder.objects.filter(**params).count() == 0:
            object = Folder.objects.create(**params)
        return object
    except Exception as e:
        print e
        return Errorquery(str(e))


def del_folder(params):
    """
    删除文件夹
    :param params:
    :return:
    """
    try:
        object = None
        Folder.objects.filter(**params).delete()
        return object
    except Exception as e:
        print e
        return Errorquery(str(e))


def info_folder(params):
    """
    文件夹详情
    :param params:
    :return:
    """
    try:
        object = Folder.objects.get(**params)
        return object
    except Exception as e:
        print e
        return Errorquery(str(e))


def update_folder(term,params):
    """
    文件夹更新
    :param params:
    :return:
    """
    try:
        object = Folder.objects.filter(**term).update(**params)
        return object
    except Exception as e:
        print e
        return Errorquery(str(e))


def list_folder(params):
    """
    文件夹列表
    :param params:
    :return:
    """
    try:
        object = Folder.objects.filter(**params).all()
        return object
    except Exception as e:
        print e
        return Errorquery(str(e))
