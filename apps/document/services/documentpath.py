# coding=utf-8
from archive_classes.models import ArchiveClasses
from common.errorcode import Errorquery
from apps.document.models import DocumentPath, FolderPath
from apps.organize_manager.models import Orginazation


def add_docpath(params):
    """
    添加文件路径
    :param params:
    :return:
    """
    try:
        object = None
        if params.get("org"):
            params["org"] = Orginazation.objects.get(id=params["org"])
        if params.get("classes"):
            params["classes"] = ArchiveClasses.objects.get(id=params["classes"])
        if DocumentPath.objects.filter(**params).count()==0:
            object = DocumentPath.objects.create(**params)
        return object
    except Exception as e:
        print e
        return Errorquery(str(e))


def del_docpath(params):
    """
    删除文件路径
    :param params:
    :return:
    """
    try:
        object = DocumentPath.objects.filter(**params).delete()
        return object
    except Exception as e:
        print e
        return Errorquery(str(e))


def info_docpath(params):
    """
    文件路径详情
    :param params:
    :return:
    """
    try:
        object = DocumentPath.objects.get(**params)
        return object
    except Exception as e:
        print e
        return Errorquery(str(e))


def update_docpath(iterm, params):
    """
    更新文件路径
    :param params:
    :return:
    """
    try:
        object = DocumentPath.objects.filter(**iterm).update(**params)
        return object
    except Exception as e:
        print e
        return Errorquery(str(e))


def list_docpath(params):
    """
    文件路径列表
    :param params:
    :return:
    """
    try:
        object = DocumentPath.objects.filter(**params).all()
        return object
    except Exception as e:
        print e
        return Errorquery(str(e))


# def path_DFS(params, folders):
#     try:
#         object = DocumentPath.objects.get(**params)
#         folders.append({"id": object.folder.id, "name": object.folder.name})
#         if object.parent is None:
#             return
#         return path_DFS({"id": object.parent_folder}, folders)
#     except Exception as e:
#         print e
#         return Errorquery(str(e))


def query_document(params):
    """
    查询文件
    :return:
    """
    result = DocumentPath.objects.filter(**params)
    return result

