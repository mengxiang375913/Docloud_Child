# coding=utf-8
from archive_classes.models import ArchiveClasses
from common.errorcode import Errorquery
from apps.document.models import FolderPath, DocumentPath
from apps.organize_manager.models import Orginazation
from documentpath import add_docpath


def add_folderpath(params):
    """
    添加文件路径
    :param params:
    :return:
    """
    try:
        object = None
        if params.get("org"):
            params["org"] = Orginazation.objects.get(id=params["org"])
        if params.get("classes"):
            params["classes"] = ArchiveClasses.objects.get(id=params["classes"])
        
        if FolderPath.objects.filter(**params).count() == 0:
            object = FolderPath.objects.create(**params)
        return object
    except Exception as e:
        print e
        return Errorquery(str(e))


def del_folderpath(params):
    """
    删除文件路径
    :param params:
    :return:
    """
    try:
        object = FolderPath.objects.filter(**params).delete()
        return object
    except Exception as e:
        print e
        return Errorquery(str(e))


def info_folderpath(params):
    """
    文件路径详情
    :param params:
    :return:
    """
    try:
        object = FolderPath.objects.get(**params)
        return object
    except Exception as e:
        print e
        return Errorquery(str(e))


def update_folderpath(iterm, params):
    """
    更新文件路径
    :param params:
    :return:
    """
    try:
        object = FolderPath.objects.filter(**iterm).update(**params)
        return object
    except Exception as e:
        print e
        return Errorquery(str(e))


def list_folderpath(params):
    """
    文件路径列表
    :param params:
    :return:
    """
    try:
        object = FolderPath.objects.filter(**params)
        return object
    except Exception as e:
        print e
        return Errorquery(str(e))


def path_DFS(params, folders):
    try:
        object = FolderPath.objects.get(**params)
        folders.append({"id": object.folder.id, "name": object.folder.name})
        if object.parent_folder is None:
            return
        return path_DFS({"folder": object.parent_folder, "classes": None}, folders)
    except Exception as e:
        print e
        return Errorquery(str(e))


def path_BFS_tree(params, level=0, max_level=65536):
    try:
        if level == max_level:
            return []
        else:
            folderpaths = list_folderpath(params)
            folderlist = []
            for folderpath in folderpaths:
                folderlist.append({"id": folderpath.folder.id, "text": folderpath.folder.name,
                                   "children": path_BFS_tree({"parent_folder": folderpath.folder, "classes": None},
                                                             level, max_level)})
            return folderlist
    except Exception as e:
        print e
        return []


def path_DFS_update(params):
    try:
        object = FolderPath.objects.get(**params)
        
        if object.parent_folder is None:
            return
        return path_DFS_update({"id": object.parent_folder.id})
    except Exception as e:
        print e
        return Errorquery(str(e))


def path_DFS_cab(params, classes):
    """
    向上归档
    :param params:
    :return:
    """
    try:
        object = FolderPath.objects.get(**params)
        add_folderpath({"classes": classes, "folder": object.folder, "parent_folder": object.parent_folder})
        if object.parent_folder is None:
            return
        return path_DFS_cab({"folder": object.parent_folder.id, "classes": None}, classes)
    except Exception as e:
        print e
        return Errorquery(str(e))


def path_UDFS_cab(params, params2, classes):
    """
    向下归档
    :param params:
    :return:
    """
    try:
        folders = FolderPath.objects.filter(**params)
        documents = DocumentPath.objects.filter(**params2)
        for folder in folders:
            print folder.folder.name
            path_UDFS_cab({"parent_folder": folder.id, "classes": None}, {"folder": folder.id}, classes)
            add_folderpath({"classes": classes, "folder": folder.folder, "parent_folder": folder.parent_folder})
        
        for document in documents:
            add_docpath(
                {"classes": classes, "folder": document.folder, "parent_folder": document.parent_folder})
            document.document.update(had_archive=True)
    except Exception as e:
        print e
        return None
