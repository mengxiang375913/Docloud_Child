# coding=utf-8
from django.db.models.query import QuerySet

from common.errorcode import Errorquery
from apps.document.models import *


def add_document(params):
    """
    添加文件
    :param params:
    :return:
    """
    try:
        if Document.objects.filter(**params).count() == 0:
            object = Document.objects.create(**params)
        return object
    except Exception as e:
        print e
        return Errorquery(str(e))


def del_document(params):
    """
    删除文件
    :param params:
    :return:
    """
    try:
        object = Document.objects.filter(**params).delete()
        return object
    except Exception as e:
        print e
        return Errorquery(str(e))


def info_document(params):
    """
    文件信息
    :param params:
    :return:
    """
    try:
        object = Document.objects.filter(**params).values()
        return object
    except Exception as e:
        print e
        return Errorquery(str(e))

def info_single_document(params):
    """
    文件信息
    :param params:
    :return:
    """
    try:
        object = Document.objects.get(**params)
        return object
    except Exception as e:
        print e
        return Errorquery(str(e))


def update_document(iterm,params):
    """
    添加文件
    :param params:
    :return:
    """
    try:
        object = Document.objects.filter(**iterm).update(**params)
        return object
    except Exception as e:
        print e
        return Errorquery(str(e))


def list_document(params):
    """
    文件列表
    :param params:
    :return:
    """
    try:
        object = Document.objects.filter(**params)
        return object
    except Exception as e:
        print e
        return Errorquery(str(e))


def query_document(document, params):
    """
    查询文件
    :return:
    """
    if isinstance(document, QuerySet) == False:
        return Errorquery("queryset is false")
    reult = document.objects.filter(**params)
    return reult
