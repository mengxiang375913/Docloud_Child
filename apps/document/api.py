# coding=utf-8
from __future__ import unicode_literals

from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r"^uploadfile$", file_upload),
    url(r"^downloadfile", file_download),
    url(r"^createdoc$", create_document),
    url(r"^createfolder$", create_folder),
    url(r"^doclist", document_list_api),
    url(r"^docsingledel", document_single_delete_api),
    url(r"^docdelete",document_delete_api),
    url(r"^query", query_single_docment),
    url(r"^filterquery", query_filter_docment),
    url(r"^archivedoc", archive_document),
    url(r"^foldertree", folder_tree),
    url(r"^docmove", doc_move_api),  # 文件移动
    url(r"^docrename", doc_rename_api),  # 文件重命名
]
