# coding=utf-8
from __future__ import unicode_literals

import uuid
from datetime import datetime

from django.db import models
from apps.organize_manager.models import *
from archive_classes.models import *


class DocumentType(models.Model):
    """
    文件类型
    """
    label = models.CharField(
        max_length=32, unique=True, verbose_name='Label'
    )
    trash_time_period = models.PositiveIntegerField(
        blank=True, null=True, verbose_name='Trash time period'
    )
    trash_time_unit = models.CharField(
        blank=True, null=True, max_length=8,
        verbose_name='Trash time unit'
    )
    delete_time_period = models.PositiveIntegerField(
        blank=True, null=True, verbose_name='Delete time period'
    )
    delete_time_unit = models.CharField(
        blank=True,
        max_length=8, null=True,
        verbose_name='Delete time unit'
    )


class Folder(models.Model):
    secrecy_group = (
        (1, "外部"),
        (2, "内部"),
        (3, "机密"),
        (4, "绝密")
    )
    # org = models.ForeignKey(Orginazation, on_delete=models.CASCADE, null=True, verbose_name="组织")
    # classes = models.ForeignKey(ArchiveClasses, on_delete=models.CASCADE, null=True, verbose_name="分类")
    uid = models.CharField(default=str(uuid.uuid4()).replace("-", ""), max_length=50, null=True)
    name = models.CharField(max_length=50, verbose_name="名称")
    create_user = models.ForeignKey(CloudAuthUser, on_delete=models.DO_NOTHING, verbose_name="创建者")
    secrecy = models.SmallIntegerField(choices=secrecy_group, default=1)
    create_time = models.DateTimeField(
        auto_now_add=True, db_index=True,
    )
    description = models.TextField(
        blank=True, default='',
    )
    in_trash = models.BooleanField(
        db_index=True, default=False, editable=False,
    )
    size = models.CharField(null=True, max_length=200, verbose_name="大小")
    parent = models.ForeignKey('self', on_delete=models.CASCADE, null=True, verbose_name="文件夹")


class Document(models.Model):
    secrecy_group = (
        (1, "外部"),
        (2, "内部"),
        (3, "机密"),
        (4, "绝密")
    )
    uid = models.CharField(default=str(uuid.uuid4()).replace("-", ""), max_length=50, null=True)
    name = models.CharField(max_length=50, verbose_name="名称")
    storage = models.CharField(max_length=200, null=True, verbose_name="存储路径")
    create_time = models.DateTimeField(auto_now_add=True, db_index=True, max_length=50)
    secrecy = models.SmallIntegerField(choices=secrecy_group, default=1)
    user_count = models.IntegerField(default=1, verbose_name="持有者数量")
    create_user = models.ForeignKey(CloudAuthUser, on_delete=models.DO_NOTHING, null=True, verbose_name="创建者")
    label = models.CharField(
        blank=True, db_index=True, default='', max_length=255,
    )
    description = models.TextField(
        blank=True, default='',
    )
    date_added = models.DateTimeField(
        auto_now_add=True, db_index=True,
    )
    in_trash = models.BooleanField(
        db_index=True, default=False, editable=False,
    )
    deleted_date_time = models.DateTimeField(
        blank=True, editable=True, null=True,
    )
    is_stub = models.BooleanField(
        db_index=True, default=True, editable=False, verbose_name='Is stub?'
    )
    had_archive = models.BooleanField(default=False, verbose_name="归档")
    size = models.CharField(null=True, max_length=200, verbose_name="大小")
    ext = models.CharField(null=True, max_length=20, verbose_name="文档类型")


class DocumnetBelong(models.Model):
    document = models.ForeignKey(Document, on_delete=models.CASCADE, verbose_name="文件")
    user = models.ForeignKey(CloudAuthUser, on_delete=models.CASCADE, verbose_name="拥有者")
    read_power = models.BooleanField(default=True)
    download_power = models.BooleanField(default=False)
    edit_power = models.BooleanField(default=False)
    delete_power = models.BooleanField(default=False)
    share_power = models.BooleanField(default=True)


class DocumentPath(models.Model):
    document = models.ForeignKey(Document, on_delete=models.CASCADE, verbose_name="文件")
    folder = models.ForeignKey(Folder, on_delete=models.CASCADE, null=True, verbose_name="文件夹")
    org = models.ForeignKey(Orginazation, null=True, on_delete=models.CASCADE, verbose_name="组织")
    classes = models.ForeignKey(ArchiveClasses, null=True, on_delete=models.CASCADE, verbose_name="分类")


class FolderPath(models.Model):
    org = models.ForeignKey(Orginazation, on_delete=models.CASCADE, null=True, verbose_name="组织")
    classes = models.ForeignKey(ArchiveClasses, on_delete=models.CASCADE, null=True, verbose_name="分类")
    folder = models.ForeignKey(Folder, related_name="child_folder", on_delete=models.CASCADE, null=True,
                               verbose_name="文件夹")
    parent_folder = models.ForeignKey(Folder, related_name="parent_folder", on_delete=models.CASCADE, null=True,
                                      verbose_name="父文件夹")
