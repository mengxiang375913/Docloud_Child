from __future__ import unicode_literals

from django.conf.urls import url
from .views import index_view

urlpatterns = [
    url(r'^login/$', index_view),
]
