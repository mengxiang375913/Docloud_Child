import md5

from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login
from apps.common.requests import *
from login.service import get_token


def index_view(request):
    if request.method == "GET":
        return render(request, template_name="login/login.html")
    else:
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            if session_setuser(request, user):
                if request.GET.get("device") is None:
                    return success_requests()
                else:
                    token = get_token()
                    return redirect("/home")#success_requests({"token":token})
            else:
                return err_requests("500")
        else:
            return err_requests("505")
# Create your views here.
