# coding=utf-8
from __future__ import unicode_literals

from django.conf import settings
from django.db import models


# Create your models here.
class Orginazation(models.Model):
    org_name = models.CharField(max_length=255, blank=True, null=True, verbose_name="组织名称")
    org_code = models.CharField(max_length=255, verbose_name="组织代码")
    parent_code = models.CharField(max_length=255, default="-")
    parent_id = models.ForeignKey("self", on_delete=models.CASCADE, null=True, verbose_name="父级")
    org_dec = models.CharField(max_length=255, null=True)
    org_sort = models.IntegerField(default=1)


class SubscriberCloudposition(models.Model):
    position_name = models.CharField(max_length=128)
    is_delete = models.IntegerField(default=0)
    remarks = models.CharField(max_length=128, null=True)


class CloudAuthUser(models.Model):
    # user1 zxcvb12345
    author = models.OneToOneField(settings.AUTH_USER_MODEL, null=True, on_delete=models.CASCADE)
    cloud_phone = models.CharField(max_length=30)
    realname = models.CharField(max_length=30, null=True)
    cloud_sex = models.CharField(max_length=30)
    cloud_status = models.IntegerField()
    cloud_is_delete = models.IntegerField(default=0)
    org = models.ForeignKey(Orginazation, models.DO_NOTHING, to_field='id', null=True)
    position = models.ForeignKey(SubscriberCloudposition, models.DO_NOTHING, blank=True, null=True,
                                 to_field='id')
