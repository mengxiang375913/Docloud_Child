# coding=utf-8
from __future__ import unicode_literals

from django.conf.urls import url
from .views import query_tree, query_org_list, query_original, query_authoriginal

urlpatterns = [
    url(r"^tree$", query_tree),
    url(r"^list$", query_org_list),
    url(r"^original$", query_original),  # 组织管理
    url(r"^authoriginal$", query_authoriginal),  # 人员管理
]
