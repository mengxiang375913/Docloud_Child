from __future__ import unicode_literals

from django.conf.urls import url
from .views import query_org_list, query_tree, add_org_info, edit_org_info, update_org_info, delete_org_info, \
    org_api_list, add_user_info, delete_user_info, edit_user_info, update_user_info, query_position_list, \
    query_user_list, authorg_api_list, add_position_api, edit_position_api, update_position_api, delete_position_api, \
    get_orguser_api

urlpatterns = [
    url(r"^orgadd$", add_org_info),
    url(r"^orgdelete$", delete_org_info),
    url(r"^orgedit$", edit_org_info),
    url(r"^orgupdate$", update_org_info),
    url(r"^orgtree$", query_tree),
    url(r"^orglist$", org_api_list),
    url(r"^authorglist$", authorg_api_list),
    url(r"^authoradd$", add_user_info),
    url(r"^authordelete$", delete_user_info),
    url(r"^authoredit$", edit_user_info),
    url(r"^authorupdate$", update_user_info),
    url(r"^authorposition$", query_position_list),
    url(r"^authorlist$", query_user_list),
    url(r"positionlist", query_position_list),
    url(r"positionadd", add_position_api),
    url(r"positiondelete", delete_position_api),
    url(r"positionedit", edit_position_api),
    url(r"positionupdate", update_position_api),
    url(r"getorguser", get_orguser_api),
]
