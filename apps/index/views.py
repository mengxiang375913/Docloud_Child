# coding=utf-8
from archive_classes.service import list_classes
from organize_manager.service import *

from django.shortcuts import render


def index(request):
    org_list = get_orgs_list()
    classes_list = list_classes(None)
    return render(request=request, template_name="home/index.html",
                  context={"org_list": org_list, "classes_list": classes_list})
# Create your views here.
