from __future__ import unicode_literals

from django.conf.urls import url
from .views import LogsModelListView

urlpatterns = [
    url(r"^list$", LogsModelListView.as_view()),
    url(r"^(?P<meta>\d+)/list$", LogsModelListView.as_view()),
]
