
from rest_framework import serializers

from logs_manager.models import Logs


class LogsSerializer(serializers.HyperlinkedModelSerializer):
    
    class Meta:
        fields = ('sort', 'id', 'name', 'value', 'type_id')
        model = Logs
        