# coding=utf-8
from __future__ import unicode_literals

from datetime import datetime

from django.db import models
from django.conf import settings


class Logs(models.Model):
    sort = models.IntegerField(null=False, verbose_name="排序")
    operate = models.CharField(max_length=20, verbose_name="操作")
    operate_remarks = models.CharField(max_length=50, verbose_name="操作描述")
    sys_model = models.CharField(max_length=50, verbose_name="模块名称")
    operater = models.CharField(max_length=20, verbose_name="操作者")
    operater_group = models.CharField(max_length=20, verbose_name="角色")  # ForeignKey
    operate_datetime = models.DateTimeField(auto_now = True, verbose_name="操作时间")
    operater_ip = models.CharField(max_length=40, verbose_name="操作网址")
