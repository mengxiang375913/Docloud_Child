# coding=utf-8
from datetime import datetime, date, timedelta

from django.db.models import Q
from django.shortcuts import render
from django.views import View

# Create your views here.
# API
from rest_framework.filters import OrderingFilter
from rest_framework.generics import ListAPIView

from common.requests import err_requests
from logs_manager.serializers import LogsSerializer


class LogsListView(ListAPIView):
    """
    列表数据
    """
    serializer_class = LogsSerializer
    filter_backends = (OrderingFilter,)
    ordering_fields = ('sort', 'id')
    
    def get_queryset(self):
        filter_meta = self.kwargs.get("meta")
        today = date.today()
        if filter_meta is None:
            Logs.objects.all()
        if filter_meta == 0:
            return Logs.objects.all()
        elif filter_meta == 1:
            return Logs.objects.filter(operate_datetime__gte=today)
        elif filter_meta == 2:
            today = today + timedelta(days=-3)
            return Logs.objects.filter(operate_datetime__gt=today)
        elif filter_meta == 3:
            today = today + timedelta(days=-7)
            return Logs.objects.filter(operate_datetime__gt=today)
        elif filter_meta == 4:
            today = today + timedelta(days=-30)
            return Logs.objects.filter(operate_datetime__gt=today)
        elif filter_meta == 5:
            today = today + timedelta(days=-30)
            return Logs.objects.filter(operate_datetime__gt=today)
        else:
            return Logs.objects.all()


# ModelView
from common.pagination import paging
from logs_manager.models import Logs


class LogsModelListView(View):
    def get_queryset(self, filter_meta, query_str, **kwargs):
        today = date.today()
        
        if filter_meta is None:
            logs = Logs.objects.all()
        
        if filter_meta == 0:
            logs = Logs.objects.all()
        elif filter_meta == 1:
            logs = Logs.objects.filter(Q(operate_datetime__gte=today))
        elif filter_meta == 2:
            today = today + timedelta(days=-3)
            logs = Logs.objects.filter(Q(operate_datetime__gt=today))
        elif filter_meta == 3:
            today = today + timedelta(days=-7)
            logs = Logs.objects.filter(Q(operate_datetime__gt=today))
        elif filter_meta == 4:
            today = today + timedelta(days=-30)
            logs = Logs.objects.filter(Q(operate_datetime__gt=today))
        elif filter_meta == 5:
            start_time = kwargs.get("start_time")
            stop_time = kwargs.get("stop_time")
            logs = Logs.objects.filter(Q(operate_datetime__gte=start_time) & Q(operate_datetime__lte=stop_time))
        else:
            logs = Logs.objects.all()
        if query_str:
            return logs.filter(Q(operate__contains=query_str) | Q(operate_remarks__contains=query_str))
        return logs
    
    def get(self, request=None, meta=None):
        try:
            query = request.GET.get("query")
            meta = int(meta)
            if meta == 5:
                start_time = request.GET.get("startTime");
                stop_time = request.GET.get("stopTime")
        except Exception as e:
            meta = 0
        
        try:
            size = int(request.GET.get("size"))
            page = int(request.GET.get("page"))
        except Exception as e:
            size = 10
            page = 1
        if meta == 5:
            if start_time and stop_time:
                logs = self.get_queryset(filter_meta=meta, query_str=query, start_time=start_time, stop_time=stop_time)
            else:
                logs = self.get_queryset(filter_meta=0, query_str=query, start_time=start_time, stop_time=stop_time)
        else:
            logs = self.get_queryset(filter_meta=meta, query_str=query)
        if logs is None:
            return err_requests("505")
        logs = logs.values()
        context = paging(logs, page, size, "logs")
        context["filter"] = meta
        context["query"] = query
        return render(request, template_name="logs_manager/logslist.html", context=context)
