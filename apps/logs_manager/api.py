from __future__ import unicode_literals

from django.conf.urls import url
from .views import LogsListView

urlpatterns = [
    url(r"^list$",LogsListView.as_view())
]
