from __future__ import unicode_literals

from django.conf.urls import url
from .views import oper_borrow, oper_return, query_borrow_all, query_return_all

urlpatterns = [
    url(r'^borrow/$', oper_borrow),
    url(r'^return/$', oper_return),
    url(r'^query_borrow_all/$', query_borrow_all),
    url(r'^query_return_all/$', query_return_all),
]
