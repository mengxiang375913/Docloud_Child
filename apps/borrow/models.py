# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# from apps.subscriber.models import CloudAuthUser


class BorrowRecord(models.Model):
    archive = models.ForeignKey(
        'archives_file.ArchivesFile', to_field='id', verbose_name='档案id',
        null=True,
    )
    borrow_time = models.DateTimeField(verbose_name="借出日期")
    return_time = models.DateTimeField(verbose_name="归还日期", null=True)
    is_delay = models.IntegerField(default=0)
    return_deadline = models.DateTimeField(verbose_name="计划归还日期")
    borrower = models.CharField(max_length=150, verbose_name="借阅人")
    # operator = models.CharField(max_length=64, verbose_name="操作人id")
    operator = models.ForeignKey(
        'organize_manager.CloudAuthUser', to_field='id',
        related_name='borrowrecord_operator'
    )
    return_operator = models.ForeignKey(
        'organize_manager.CloudAuthUser', to_field='id',
        related_name='return_operator', null=True
    )
    status = models.CharField(max_length=64, verbose_name="状态")

    class Meta:
        db_table = 'borrow_record'

    def __unicode__(self):
        return str(self.id)


class ReturnRecord(models.Model):
    archive = models.ForeignKey(
        'archives_file.ArchivesFile', to_field='id', verbose_name='档案id',
        null=True,
    )
    return_time = models.DateTimeField(verbose_name="归还日期")
    return_deadline = models.DateTimeField(verbose_name="计划归还日期")
    operator = models.ForeignKey(
        'organize_manager.CloudAuthUser', to_field='id',
        related_name='returnrecord_operator'
    )
    is_delay = models.CharField(max_length=64, verbose_name="是否延期")

    class Meta:
        db_table = 'return_record'

    def __unicode__(self):
        return str(self.id)
