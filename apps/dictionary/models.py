# coding=utf-8
from __future__ import unicode_literals

from django.db import models


class DictionaryType(models.Model):
    
    code = models.CharField(max_length=255, verbose_name="编码")
    status = models.IntegerField(default=0, verbose_name="状态")
    name = models.CharField(max_length=255, verbose_name="字典类型名称")


class DictionaryItem(models.Model):
    sort = models.IntegerField(default=0, verbose_name="序号")
    code = models.CharField(max_length=255, verbose_name="编码",null=True)
    name = models.CharField(max_length=255, verbose_name="字典内容")
    value = models.IntegerField(default=0, verbose_name="值")
    type = models.ForeignKey(DictionaryType, on_delete=models.CASCADE, verbose_name="字典类型")

# Create your models here.
