from __future__ import unicode_literals

from django.conf.urls import url
from .views import DictionaryTypeModelListView, DictionaryItemModelListView

urlpatterns = [
    url(r"^type/(?P<pk>\d+)/list$", DictionaryTypeModelListView.as_view()),
    url(r"^type/list$", DictionaryTypeModelListView.as_view()),
    url(r"^type/create", DictionaryTypeModelListView.as_view()),
    url(r"^type/delete$", DictionaryTypeModelListView.as_view()),
    url(r"^type/edit$", DictionaryTypeModelListView.as_view()),
    url(r"^item/(?P<pk>\d+)/list", DictionaryItemModelListView.as_view())
]
