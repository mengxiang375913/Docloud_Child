# coding=utf-8
from __future__ import unicode_literals

from django.conf.urls import url
from .views import DictionaryTypeCreateView, DictionaryTypeListView, DictionaryTypeModelListView, \
    DictionaryTypeDeleteView, DictionaryTypeEditView, DictionaryCreateView, DictionaryDetailView, DictionaryEditView, \
    DictionaryDeleteView

urlpatterns = [
    url(r"^type/create$", DictionaryTypeCreateView.as_view()),
    url(r"^type/list$", DictionaryTypeListView.as_view()),
    url(r"^type/(?P<pk>\d+)/delete/$", DictionaryTypeDeleteView.as_view()),
    url(r"^type/(?P<pk>\d+)/edit$", DictionaryTypeEditView.as_view()),
    url(r"^type/detail$", DictionaryTypeModelListView.as_view()),
    
    # 数据字典接口
    url(r"^create$", DictionaryCreateView.as_view()),
    url(r"^detail$", DictionaryDetailView.as_view()),
    url(r"^edit$", DictionaryEditView.as_view()),
    url(r"^delete$", DictionaryDeleteView.as_view()),
    # url(r"item/list",DictionaryItemListView.as_view())

]
