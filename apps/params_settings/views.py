# coding=utf-8
from django.http import JsonResponse
from django.shortcuts import render, redirect

import models



# def get_all_stage(request):
#     # type: (object) -> object
#     if request.method == "GET":
#         objects_all = models.Classes.objects.all()
#         objects_all_json = eval(objects_all)
#         html_str = ""
#         for item in objects_all_json:
#             str_stage_info = item.get("stage_info")
#             html_str += jsostr_to_html_str(str_stage_info)+"\n"
#         return render(request, "/show_param_setting.html", {"obj": html_str})



def get_all_stage(request):
    # type: (object) -> object
    # if request.method == "GET":
    try:
        objects_all = models.TblParamsSetting.objects.all()
        result_object = {}
        for item in objects_all:
            print(item)
            stage_name_ = item.stage_name
            stage_info_ = item.stage_info
            stage_info_ = eval(stage_info_)
            stage_id = item.id
            stage_info_['stage_id'] = stage_id
            result_object[stage_name_] = stage_info_
        return render(request, "params_setting/setParam.html", {"params_dict": result_object})
    except Exception as e:
        print(e)


def edit_stage(request):
    if request.method == "POST":
        try:
            stage_id = request.POST.get("stage_id")
            print "stage_id is %s" % stage_id
            if stage_id:
                stage_name = request.POST.get("stage_name")
                stage_info = request.POST.get("stage_info")
                print "stage_name is %s" % stage_name
                print "stage_info is %s" % stage_info
                # 如果是一个json则转换为一个json字符串，否则前端传入参数不正确
                stage_info = eval(stage_info)
                stage_info = str(stage_info)
                if stage_name:
                    models.TblParamsSetting.objects.filter(id=stage_id).update(stage_name=stage_name)
                    print("ccccccccccccccccc")
                if stage_info:
                    print("xxxxxxxxxxxxxxxxxxx")
                    models.TblParamsSetting.objects.filter(id=stage_id).update(stage_info=stage_info)
                data = {"code": 200, "message": None, "success": 0}
                return JsonResponse(data)
            else:
                data = {"code": 500, "message": None, "success": -1}
                return JsonResponse(data)
        except Exception as e:
            print(e)
            data = {"code": 500, "message": None, "success": -2}
            return JsonResponse(data)
    else:
        data = {"code": 500, "message": None, "success": -3}
        return JsonResponse(data)


# def create_stage(request):
#     if request.method == "POST":
#         try:
#             stage_name = request.POST.get("stage_name")
#             stage_info = request.POST.get("stage_info")
#             # 如果是一个json则转换为一个json字符串，否则前端传入参数不正确
#             stage_info = eval(stage_info)
#             stage_info = str(stage_info)
#             params_setting_object = models.TblParamsSetting(stage_name=stage_name, stage_info=stage_info)
#             params_setting_object.save()
#             return redirect('paramsSettings/setParam.html')
#         except Exception as e:
#             print(e)
#
#
# def delete_stage(request):
#     if request.method == "DELETE":
#         try:
#             stage_id = request.DELETE.get("stage_id")
#             if stage_id:
#                 models.TblParamsSetting.objects.filter(id=stage_id).delete()
#                 return redirect('paramsSettings/setParam.html')
#         except Exception as e:
#             print(e)
#
#
# def err_requests(code, message=None):
#     if message is None:
#         message = response_error.get(code)
#     data = {"code": code, "message": message, "success": 0}
#     return JsonResponse(data)
#
#
# def success_requests(data=None):
#     data = {"code": 1, "message": "success", "success": 1, "data": data}
#     return JsonResponse(data)
