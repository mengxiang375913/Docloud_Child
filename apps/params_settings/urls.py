# coding=utf-8
from django.conf.urls import url

from .views import get_all_stage, edit_stage

urlpatterns = [
    url(r"list", get_all_stage),
    url(r"edit", edit_stage),
    # url(r"create", create_stage),
    # url(r"delete", delete_stage)
    ]
