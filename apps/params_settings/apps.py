from __future__ import unicode_literals

from django.apps import AppConfig


class ParamsSettingConfig(AppConfig):
    name = 'params_settings'
