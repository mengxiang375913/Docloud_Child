# -*- coding: utf-8 -*-
"""
档案柜编辑所需SQL语句集中
@Author: HuangYu
@E-mail: huangyu@dinton.cn
"""
# 查询档案柜信息
QUERY_CABINET = """
    SELECT
        `id`, `code`, `material`, `storage_location`, `remark`
    FROM
        `archive_cabinet`
    WHERE
        1 = 1
"""

# 编辑档案柜基本属性
SET_CABINET = """
    UPDATE `archive_cabinet` SET
        `code` = %s,
        `material` = %s,
        `storage_location` = %s,
        `remark` = %s,
        `admin_id` = %s
    WHERE
        1 = 1
"""

# 查询用户信息
QUERY_AUTHOR = """
    SELECT
        `id`, `username`
    FROM
        `cloud_auth_user`
    WHERE
        1 = 1
"""

# 查询档案盒
QUERY_BOX = """
    SELECT
        `id`, `code`, `specifications`, `remark`, `lattice_id`
    FROM
        `archive_box`
    WHERE
        1 = 1
"""

# 编辑档案盒基本属性
SET_BOX = """
    UPDATE `archive_box` SET
        `code` = %s,
        `specifications` = %s,
        `remark` = %s
    WHERE
        1 = 1
"""

# 查询档案隔档个数
QUERY_LATTICE_COUNT = """
    SELECT
        COUNT(DISTINCT `id`)
    FROM
        `archive_lattice`
    WHERE
        1 = 1
"""

# 新增档案隔档
INSERT_LATTICE = """
    INSERT INTO `archive_lattice` (
        `code`, `cabinet_id`
    )
    VALUES (
        %s, %s
    )
"""

# 删除档案盒
DEL_BOX = "DELETE FROM `archive_box` WHERE 1 = 1"

# 删除档案隔档
DEL_LATTICE = "DELETE FROM `archive_lattice` WHERE 1 = 1"

# 删除档案柜
DEL_CABINET = "DELETE FROM `archive_cabinet` WHERE 1 = 1"
