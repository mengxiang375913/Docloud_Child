# -*- coding: utf-8 -*-
"""
@Author: HuangYu
@E-mail: huangyu@dinton.cn
"""
import traceback

from django.db import connection
from django.http import JsonResponse

from apps.archive_cabinets.dataview.sql.mysql import (
    edit_cabinet_sql as edit_sql
)
from apps.common.errorcode import response_error, operate_error
from apps.archive_cabinets.models import *
from apps.common.requests import *


def del_box(request):
    """档案盒的删除接口"""
    try:
        body = get_body(request)
        if isinstance(body, JsonResponse):
            return body

        box_params = get_params(body, {"id": "id"})
        if isinstance(box_params, JsonResponse):
            return box_params

        Box.objects.filter(id=box_params.get("id")).delete()
        return success_requests()

    except Exception as e:
        traceback.print_exc()
        return err_requests("500", str(e))


def del_lattice(request):
    """档案隔档删除接口"""
    try:
        body = get_body(request)
        if isinstance(body, JsonResponse):
            return body

        lattice_params = get_params(body, {"id": "id"})
        if isinstance(lattice_params, JsonResponse):
            return lattice_params

        if Box.objects.filter(lattice=lattice_params.get("id")).count()>0:
            return err_requests("500")

        Lattice.objects.filter(id=lattice_params.get("id")).delete()

        return success_requests()

    except Exception as e:
        traceback.print_exc()
        return err_requests("500", str(e))


def del_cabinet(request):
    """档案柜删除接口"""
    try:
        body = get_body(request)
        if isinstance(body, JsonResponse):
            return body

        cabinet_params = get_params(body, {"id": "id"})
        if isinstance(cabinet_params, JsonResponse):
            return cabinet_params

        Cabinet.objects.filter(id=cabinet_params.get("id")).delete()

        return success_requests()
    except Exception as e:
        traceback.print_exc()
        return err_requests("500", str(e))
