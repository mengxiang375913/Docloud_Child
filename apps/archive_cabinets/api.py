# -*- coding=utf-8 -*-
from django.conf.urls import url
from apps.archive_cabinets import (views)
from archive_cabinets import cabinet_del, cabinets_edit

urlpatterns = [
    url(r'^addCabinet$', views.add_cabinet),
    url(r'^inquireCabinetsList$', views.inquire_cabinets_list),
    url(r'^inquireCabinetsUpdate$', views.inquire_cabinets_update),
    url(r'^inquireCabinetDetail$', views.inquire_cabinet_detail),
    url(r'^inquireLatticesList$', views.inquire_lattices_list),
    url(r'^inquireBoxDetail$', views.inquire_box_detail),
    url(r'^inquireBoxUpdate$', views.inquire_box_update),
    url(r'^inquireBoxesList$', views.inquire_boxes_list),
    url(r'^addCabinetBox$', views.add_cabinet_box),
    url(r'^editCabinet$', cabinets_edit.edit_cabinet),
    url(r'^editBox$', cabinets_edit.edit_box),
    url(r'^delBox$', cabinet_del.del_box),
    url(r'^delLattice$', cabinet_del.del_lattice),
    url(r'^delCabinets$', cabinet_del.del_cabinet),
]
