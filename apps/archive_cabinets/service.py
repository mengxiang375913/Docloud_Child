# coding=utf-8
from common.errorcode import *
from models import Cabinet, Lattice
from apps.organize_manager.models import CloudAuthUser


def create_cabinet(body):
    try:
        lattices_num = int(body.get("latticesNum"))
        del body["latticesNum"]
        if body.get("admin"):
            body["admin"] = CloudAuthUser.objects.get(id = body["admin"])
        cabinet = Cabinet.objects.create(**body)
        for i in range(lattices_num):
            Lattice.objects.create(code = "Num{}".format(i+1),cabinet=cabinet)
        return cabinet
    except Exception as e:
        print e
        return Errorquery(e)
    
def update_cabinet(body):
    try:
        del body["latticesNum"]
        if body.get("admin"):
            body["admin"] = CloudAuthUser.objects.get(id = body["admin"])
        cabinet = Cabinet.objects.filter(id=body["id"]).update(**body)
        return cabinet
    except Exception as e:
        print e
        return Errorquery(e)
