# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import time
import uuid

from django.db import models
from django.conf import settings

# Create your models here.
class Cabinet(models.Model):
    """档案柜的数据库模型"""
    code = models.CharField(
        max_length=255, unique=True, verbose_name='编号'
    )
    material = models.CharField(max_length=50, verbose_name='材质', null=True)
    storage_location = models.CharField(max_length=100, verbose_name='存放位置', null=True)
    remark = models.CharField(max_length=200, verbose_name='备注', null=True)
    admin = models.ForeignKey(
        "organize_manager.CloudAuthUser", on_delete=models.SET_NULL, verbose_name='管理员', null=True
    )
    
    class Meta:
        db_table = 'archive_cabinet'
    
    def __unicode__(self):
        return str(self.code)


class Lattice(models.Model):
    """档案隔档的数据库模型"""
    code = models.CharField(
        max_length=255, verbose_name='编号', default=str(uuid.uuid4()).split("-")[0]
    )
    cabinet = models.ForeignKey(
        'Cabinet', verbose_name='档案柜'
    )
    
    class Meta:
        db_table = 'archive_lattice'
        unique_together = (("code", "cabinet"),)
    
    def __unicode__(self):
        return str(self.code)


class Box(models.Model):
    """档案盒的数据库模型"""
    code = models.CharField(
        max_length=255, verbose_name='编号'
    )
    specifications = models.CharField(max_length=50, verbose_name='规格',null=True)
    remark = models.CharField(max_length=200, verbose_name='备注')
    lattice = models.ForeignKey(
        'Lattice', verbose_name='隔档'
    )
    
    class Meta:
        db_table = 'archive_box'
        unique_together = (("code", "lattice"),)
    
    def __unicode__(self):
        return str(self.code)
