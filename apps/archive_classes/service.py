# coding=utf-8
from archive_classes.models import ArchiveClasses
from common.errorcode import Errorquery


def add_classes(params):
    """
    添加
    :param params:
    :return:
    """
    try:
        object = None
        return object
    except Exception as e:
        print e
        return Errorquery(str(e))


def del_classes(params):
    """
    删除
    :param params:
    :return:
    """
    try:
        object = None
        return object
    except Exception as e:
        print e
        return Errorquery(str(e))


def info_classes(params):
    """
    详情
    :param params:
    :return:
    """
    try:
        return object
    except Exception as e:
        print e
        return Errorquery(str(e))


def update_classes(params):
    """
    更新
    :param params:
    :return:
    """
    try:
        object = None
        id = params["id"]
        del params["id"]
        return object
    except Exception as e:
        print e
        return Errorquery(str(e))


def list_classes(params):
    """
    列表
    :param params:
    :return:
    """
    try:
        if params is None:
            object = ArchiveClasses.objects.all().order_by('sort', 'id')
        else:
            object = ArchiveClasses.objects.filter(**params).all().order_by('sort', 'id')
        return object
    except Exception as e:
        print e
        return Errorquery(str(e))
