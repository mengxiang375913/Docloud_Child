# coding=utf-8
from __future__ import unicode_literals

from django.db import models


class ArchiveClasses(models.Model):
    name = models.CharField(max_length=20,verbose_name="名称")
    code = models.CharField(max_length=40,verbose_name="编码")
    sort = models.IntegerField(verbose_name="排序",default=1)


class ClassesKeyWord(models.Model):
    word = models.CharField(max_length=20,verbose_name="索引词")
    archive_id = models.ForeignKey(ArchiveClasses, on_delete=models.CASCADE,verbose_name="文件类")
# Create your models here.
