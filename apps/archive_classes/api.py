# coding=utf-8
from __future__ import unicode_literals

from django.conf.urls import url
from .views import *

urlpatterns = [
    # 分类操作
    url(r"^info$", classes_info_api),
    url(r"^update$", classes_update_api),
    url(r"^add$", classes_add_api),
    url(r"^delete$", classes_delete_api),
    url(r"^list$", classes_list_api),
    
    url(r"defaultArchive$",default_archive_api),
    url(r"smartArchive$", smart_archive_api),

]
