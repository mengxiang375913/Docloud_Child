# coding=utf-8
def paging(data_list, page, size, result="result"):
    """
    分页
    :param data_list:数据列表
    :param page:页数
    :param size:页长度
    :return:
    """
    size = int(size)
    page = int(page)
    ret = {
        'page': page, 'size': size, 'total_page': 0, 'total_size': 0
    }
    if data_list.count() ==0:
        ret[result] = None
        return ret
    total_size = data_list.count()
    if total_size % size == 0:
        total_page = total_size / size
    else:
        total_page = total_size / size + 1
    start_index = (page - 1) * size
    end_index = page * size
    
    ret['total_page'] = total_page
    ret['total_size'] = total_size
    if end_index > len(data_list):
        ret[result] = data_list[start_index:]
        ret["end"] = False
    else:
        ret["end"] = True
        ret[result] = data_list[start_index: end_index]
    return ret
