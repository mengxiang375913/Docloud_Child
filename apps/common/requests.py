# coding=utf-8
import json

from django.core.cache import cache
from django.http import JsonResponse
from common.errorcode import response_error, Errorquery
from organize_manager.service import get_user_id, get_user_auther


def get_body(request):
    """
    获得返回数值
    :param request:
    :return:
    """
    try:
        body = json.loads(request.body)
        return body
    except Exception as e:
        print e
        return err_requests("505")


def get_params(body, params=None, null_params=None, **replace_null):
    """
    序列化请求
    :param body:dict
    :param params:dict 必填内容
    :param null_params:dict 可以为空内容
    :param replace_null: 为空以后填补的内容
    :return:
    """
    try:
        params_dict = {}
        if params:
            for key, param in params.items():
                params_dict[key] = body.get(param)
                if not all(params_dict):
                    return err_requests("505", "'{}' params is null".format(param))
        if null_params:
            for key, null_param in null_params.items():
                value = body.get(null_param)
                if not value:
                    params_dict[key] = replace_null.get(null_param)
                else:
                    params_dict[key] = value
        return params_dict
    except Exception as e:
        print e
        return err_requests("505")


def get_params_exitnull(body, params=None, null_params=None, **replace_null):
    """
    序列化请求
    :param body:dict
    :param params:dict 必填内容
    :param null_params:dict 可以为空内容
    :param replace_null: 为空以后填补的内容
    :return:
    """
    try:
        params_dict = {}
        if params:
            for key, param in params.items():
                params_dict[key] = body.get(param)
                if not all(params_dict):
                    return err_requests("505", "'{}' params is null".format(param))
        if null_params:
            for key, null_param in null_params.items():
                value = body.get(null_param)
                if not value:
                    value = replace_null.get(null_param)
                    if value:
                        params_dict[key] = value
                else:
                    params_dict[key] = value
        return params_dict
    except Exception as e:
        print e
        return err_requests("505")


def err_requests(code, message=None):
    """
    错误请求响应
    :param code:
    :param message:
    :return:
    """
    if message is None:
        message = response_error.get(code)
    data = {"code": code, "message": message, "success": 0}
    return JsonResponse(data)


def success_requests(data=None):
    """
    成功请求响应
    :param data:
    :return:
    """
    data = {"code": 1, "message": "success", "success": 1, "data": data}
    return JsonResponse(data)


def session_getuser(request):
    uuid = request.GET.get("uuid")
    if uuid:
        user_id = cache.get(uuid)
    else:
        user_id = request.session.get("user_id")
    user = get_user_id(user_id)
    if user is None:
        return Errorquery("user is null")
    return user


def session_setuser(request, user):
    user = get_user_auther(user)
    if user is None:
        return False
    else:
        if request.GET.get("device") is None:
            request.session["user_id"] = user.id
            request.session["is_login"] = True
    return True
