# encoding=utf-8
from __future__ import unicode_literals

from django.db import models

from apps.borrow.models import BorrowRecord
from apps.organize_manager.models import CloudAuthUser
from apps.archive_cabinets.models import Cabinet, Box
from apps.archive_cabinets.models import Lattice


#
#
# # 待删除
# class Cabinet(models.Model):
#     code = models.IntegerField(default=0, verbose_name="编号", unique=True)
#     lattice = models.IntegerField(default=0, verbose_name="格子", unique=True)
#     material = models.CharField(max_length=255, verbose_name="材质")
#     storage_locations = models.CharField(max_length=255, verbose_name="存放位置")
#     remark = models.CharField(max_length=255, verbose_name="备注")
#     administrator = models.CharField(max_length=255, verbose_name="管理员")
#
#     class Meta:
#         managed = False
#         db_table = 'cabinet_cabinet'
#
#
# # 盒子 待删除
# class Box(models.Model):
#     code = models.IntegerField(default=0, verbose_name="盒号", unique=True)
#     specifications = models.IntegerField(default=0, verbose_name="规格")
#     remark = models.CharField(max_length=255, verbose_name="备注")
#     lattice = models.ForeignKey(Cabinet, related_name='box_lattice')
#
#     class Meta:
#         managed = False
#         db_table = 'cabinet_box'
#
#
# # 用户组织关系 待删除
# class AuthUser(models.Model):
#     password = models.CharField(max_length=128)
#     last_login = models.DateTimeField(blank=True, null=True)
#     is_superuser = models.IntegerField()
#     username = models.CharField(unique=True, max_length=150)
#     first_name = models.CharField(max_length=30)
#     last_name = models.CharField(max_length=30)
#     email = models.CharField(max_length=254)
#     is_staff = models.IntegerField()
#     is_active = models.IntegerField()
#     date_joined = models.DateTimeField()
#
#     class Meta:
#         managed = False
#         db_table = 'auth_user'
#
#
# # 待删除
# class CloudAuthUser(models.Model):
#     password = models.CharField(max_length=128)
#     last_login = models.DateTimeField(blank=True, null=True)
#     is_superuser = models.IntegerField()
#     username = models.CharField(unique=True, max_length=150)
#     first_name = models.CharField(max_length=30)
#     last_name = models.CharField(max_length=30)
#     email = models.CharField(max_length=254)
#     is_staff = models.IntegerField()
#     is_active = models.IntegerField()
#     cloud_phone = models.CharField(max_length=30)
#     cloud_sex = models.CharField(max_length=30)
#     cloud_status = models.IntegerField()
#     cloud_is_delete = models.IntegerField()
#     org = models.ForeignKey('OrginazationOrginazation', models.DO_NOTHING, to_field='id')
#     position = models.ForeignKey('SubscriberCloudposition', models.DO_NOTHING, blank=True, null=True,
#                                  to_field='id')
#
#     class Meta:
#         managed = False
#         db_table = 'cloud_auth_user'
#
#
# # 待删除
# class OrginazationOrginazation(models.Model):
#     org_name = models.CharField(max_length=255, blank=True, null=True)
#     org_code = models.CharField(max_length=255)
#     parent_code = models.CharField(max_length=255)
#     parent_id = models.IntegerField()
#     org_dec = models.CharField(max_length=255)
#
#     class Meta:
#         managed = False
#         db_table = 'orginazation_orginazation'
#
#
# # 待删除
# class SubscriberCloudposition(models.Model):
#     position_name = models.CharField(max_length=128)
#     is_delete = models.IntegerField()
#
#     class Meta:
#         managed = False
#         db_table = 'subscriber_cloudposition'


# 待删除 借阅记录
# Create your models here.

#
#
# class BorrowRecord(models.Model):
#     serial = models.CharField(max_length=255, verbose_name="档号")
#     title = models.CharField(max_length=255, verbose_name="题名")
#     borrow_time = models.DateTimeField(verbose_name="借出日期")
#     return_deadline = models.DateTimeField(verbose_name="计划归还日期")
#     borrow_user_name = models.CharField(max_length=150, verbose_name="借阅人姓名")
#     operator = models.CharField(max_length=64, verbose_name="操作人id")
#     status = models.CharField(max_length=64, verbose_name="状态")
#
#     class Meta:
#         managed = False
#         db_table = 'borrow_borrowrecord'


class ArchivesFile(models.Model):
    serial = models.CharField(unique=True, max_length=255, verbose_name='档号')
    title = models.CharField(max_length=255, verbose_name='题名')
    storage_type = models.CharField(max_length=64, verbose_name='类型')
    storage_deadline = models.CharField(max_length=64, verbose_name='保管期限')
    file_status = models.CharField(max_length=64, verbose_name='状态')
    storage_time = models.DateTimeField(
        auto_now_add=True, verbose_name='入库时间'
    )
    storage_method = models.CharField(max_length=64, verbose_name='入库方式')
    operator = models.ForeignKey(
        CloudAuthUser, models.DO_NOTHING, to_field='id',null=True,
        related_name='archivesfile_operator', verbose_name='操作人'
    )
    owner = models.ForeignKey(
        CloudAuthUser, models.DO_NOTHING, to_field='id',null=True,
        related_name='archivesfile_owner', verbose_name='责任人'
    )
    borrowing_record = models.ForeignKey(
        BorrowRecord, models.DO_NOTHING, to_field='id',
        related_name='archivesfile_borrow', verbose_name='借阅记录', null=True
    )
    secret_level = models.CharField(max_length=64, verbose_name='密级')
    photo = models.CharField(
        max_length=255, blank=True, null=True, verbose_name='图片'
    )
    cabinet = models.ForeignKey(
        Cabinet, models.DO_NOTHING, to_field='id',null=True,
        related_name='archivesfile_cabinet', verbose_name='档案柜'
    )
    lattice = models.ForeignKey(
        Lattice, models.DO_NOTHING, to_field='id',null=True,
        related_name='archivesfile_lattice', verbose_name='档案隔档'
    )
    box = models.ForeignKey(
        Box, models.DO_NOTHING, to_field='id', related_name='archivesfile_box',
        verbose_name='档案盒',null=True
    )

    def __unicode__(self):
        return '%s' % (self.serial)

    @property
    def location(self):
        return '%s-%s-%s' % (self.cabinet.code, self.lattice.code, self.box.code)

    @property
    def operator_username(self):
        return '%s' % self.operator.realname

    @property
    def owner_username(self):
        return '%s' % self.owner.realname

    @property
    def department_name(self):
        return "%s" % self.owner.org.org_name

    class Meta:
        # managed = False
        db_table = 'archives_file'


