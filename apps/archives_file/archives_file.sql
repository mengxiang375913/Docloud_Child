/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50553
Source Host           : localhost:3306
Source Database       : dangan_cloud

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2019-02-01 14:51:21
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `archives_file`
-- ----------------------------
DROP TABLE IF EXISTS `archives_file`;
CREATE TABLE `archives_file` (
  `id` int(11)  primary key AUTO_INCREMENT,
  `serial` varchar(255) unique NOT NULL,
  `title` varchar(255) NOT NULL,
  `storage_type` varchar(64) NOT NULL,
  `storage_deadline` varchar(64) NOT NULL,
  `file_status` varchar(64) NOT NULL,
  `storage_time` timestamp NOT NULL DEFAULT now(),
  `storage_method` varchar(64) NOT NULL,
  `operator_id` int(11) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `borrowing_record_id` int(11) DEFAULT -1,
  `secret_level` varchar(64) NOT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `cabinet_id` int(11) NOT NULL,
  `lattice_id` int(11) NOT NULL,
  `box_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of archives_file
-- ----------------------------



insert into archives_file (`serial`,`title`,`storage_type`,`storage_deadline`,
`file_status`,`storage_method`,`owner_id`,`operator_id`,`secret_level`,`photo`,`cabinet_id`,`lattice_id`,`box_id`,`storage_time`) values
('YJ-2019-WH-D10-1001','中国电信云计算（陕西）基地IDC机房入室申请单','工单'
,'10年','在库','数字化入库',1,2,'公开','url1,url2.url3,url4,url5,url6',1,1,1,now());


insert into archives_file (`serial`,`title`,`storage_type`,`storage_deadline`,
`file_status`,`storage_method`,`owner_id`,`operator_id`,`secret_level`,`borrowing_record_id`,`cabinet_id`,`lattice_id`,`box_id`,`storage_time`) values
('YJ-2019-YUN-D10-1001','IDC增值服务工单','工单'
,'永久','借出','纸质入库',1,2,'公开',1,1,1,2,now());
select * from archives_file;


