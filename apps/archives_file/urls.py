from __future__ import unicode_literals

from django.conf.urls import url
from .views import files, file_add, file_update, file_del, files_json

urlpatterns = [
    url(r'^file/list$', files),
    url(r'^file/data$', files_json),
    url(r"^file/add/$", file_add),
    url(r"^file/update/$", file_update),
    url(r"^files/del/$", file_del),
    # url(r"^files/(?P<serial>.*?-.*?-.*?-.*?-.*?)/$", file_operation_path),
]
