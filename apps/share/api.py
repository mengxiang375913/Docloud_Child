from __future__ import unicode_literals

from django.conf.urls import url
from views import *

urlpatterns = [

    url(r"^adddoc", add_docshare_api),
    url(r"^deldoc", del_docshare_api),
    url(r"^infodoc", info_docshare_api),
    url(r"^listdoc", list_docshare_api),
    url(r"^updatedoc", update_docshare_api),
    url(r"^myshare", myshare_view),
    url(r"^sharewith", sharetome_view),

    url(r"^addfolder", add_docshare_api),
    url(r"^delfolder", del_docshare_api),
    url(r"^infofolder", info_docshare_api),
    url(r"^listfolder", list_docshare_api),
    url(r"^updatefolder", update_docshare_api),

    url(r"^myshare", myshare_view),
    url(r"^sharetome", sharetome_view),
]
