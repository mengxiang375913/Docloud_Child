# coding=utf-8
from common.errorcode import Errorquery
from apps.share.models import *


def add_docshare(params):
    """
    添加文件分享
    :param params:
    :return:
    """
    try:
        object = DocumentShare.objects.create(**params)
        return object
    except Exception as e:
        print e
        return Errorquery(str(e))


def del_docshare(params):
    """
    删除文件分享
    :param params:
    :return:
    """
    try:
        object = DocumentShare.objects.filter(**params).delete()
        return object
    except Exception as e:
        print e
        return Errorquery(str(e))


def info_docshare(params):
    """
    文件分享详情
    :param params:
    :return:
    """
    try:
        object = DocumentShare.objects.get(**params)
        return object
    except Exception as e:
        print e
        return Errorquery(str(e))


def update_docshare(term, params):
    """
    更新文件分享
    :param params:
    :return:
    """
    try:
        object = DocumentShare.objects.filter(**term).update(**params)
        return object
    except Exception as e:
        print e
        return Errorquery(str(e))


def list_docshare(params):
    """
    文件分享列表
    :param params:
    :return:
    """
    try:
        object = DocumentShare.objects.filter(**params).all()
        return object
    except Exception as e:
        print e
        return Errorquery(str(e))


def share_with_user(params):
    pass


def share_with_org(params):
    pass
