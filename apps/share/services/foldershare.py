# coding=utf-8
from common.errorcode import Errorquery
from apps.share.models import *

def add_foldershare(params):
    """
    添加文件夹分享
    :param params:
    :return:
    """
    try:
        object = FolderShare.objects.create(**params)
        return object
    except Exception as e:
        print e
        return Errorquery(str(e))


def del_foldershare(params):
    """
    删除文件夹分享
    :param params:
    :return:
    """
    try:
        object = FolderShare.objects.filter(**params).delete()
        return object
    except Exception as e:
        print e
        return Errorquery(str(e))


def info_foldershare(params):
    """
    文件夹分享详情
    :param params:
    :return:
    """
    try:
        object = FolderShare.objects.get(**params)
        return object
    except Exception as e:
        print e
        return Errorquery(str(e))


def update_foldershare(term,params):
    """
    更新文件夹分享
    :param params:
    :return:
    """
    try:
        object = FolderShare.objects.filter(**term).update(**params)
        return object
    except Exception as e:
        print e
        return Errorquery(str(e))


def list_foldershare(params):
    """
    文件夹分享列表
    :param params:
    :return:
    """
    try:
        object = FolderShare.objects.filter(**params).all()
        return object
    except Exception as e:
        print e
        return Errorquery(str(e))
