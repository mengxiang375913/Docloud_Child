# coding=utf-8
from django.shortcuts import render
from apps.common.requests import *
from apps.document.services.document import info_document, info_single_document
from organize_manager.service import get_all_orguser, get_all_org
from common.requests import *
from services.documentshare import *
from services.foldershare import *


def myshare_view(request):
    """
    我的分享视图
    :param request:
    :return:
    """
    if request.method == "GET":
        return render(request, template_name="share/myshare.html")
    else:
        auth = session_getuser(request)
        if isinstance(auth, Errorquery):
            return err_requests(auth.msg)
        shares = list_docshare({"user_from": auth})
        share_list = []
        for share in shares:
            share_object = {}
            share_object["title"] = share.document.name
            share_object["share_time"] = share.share_time
            share_object["size"] = share.document.size
            share_object["share_from"] = share.user_from.realname
            share_object["id"] = share.id
            share_object["uid"] = share.document.uid
            share_object["doc_id"] = share.document.id
            share_list.append(share_object)
        return success_requests(share_list)


def sharetome_view(request):
    """
    分享给我的视图
    :param request:
    :return:
    """
    if request.method == "GET":
        return render(request, template_name="share/sharetome.html")
    else:
        auth = session_getuser(request)
        if isinstance(auth, Errorquery):
            return err_requests(auth.msg)
        shares = list_docshare({"user_to": auth})
        share_list = []
        for share in shares:
            share_object = {}
            share_object["title"] = share.document.name
            share_object["share_time"] = share.share_time
            share_object["size"] = share.document.size
            share_object["share_from"] = share.user_from.realname
            share_object["id"] = share.id
            share_object["uid"] = share.document.uid
            share_object["doc_id"] = share.document.id
            share_list.append(share_object)
        return success_requests(share_list)


def add_docshare_api(request):
    """
    添加文件分享
    :param request:
    :return:
    """
    try:
        body = get_body(request)
        if isinstance(body, JsonResponse):
            return body
        params = get_params(body, {"level": "treeLevel", "id": "treeID", "docid": "fileID"})
        if isinstance(params, JsonResponse):
            return params
        document = info_single_document({"id": params["docid"]})
        auth = session_getuser(request)
        if isinstance(auth, Errorquery):
            return err_requests(auth.msg)

        if params["level"] == 2:
            users = get_all_org({"id": params["id"]})
            for user in users:
                add_docshare({"user_from": auth, "user_to": user, "document": document})
        elif params["level"] == 3:
            user = get_user_id(params["id"])
            add_docshare({"user_from": auth, "user_to": user, "document": document})

        return success_requests()
    except Exception as e:
        print e
        return err_requests("500", str(e))


def del_docshare_api(request):
    try:
        body = get_body(request)
        if isinstance(body, JsonResponse):
            return body
        params = get_params(body, {"id": "id"})
        if isinstance(params, JsonResponse):
            return params
        result = del_docshare(params)
        return success_requests(result)
    except Exception as e:
        print e
        return err_requests("500", str(e))


def info_docshare_api(request):
    try:
        body = get_body(request)
        if isinstance(body, JsonResponse):
            return body
        params = get_params(body, {})
        if isinstance(params, JsonResponse):
            return params
        return success_requests()
    except Exception as e:
        print e
        return err_requests("500", str(e))


def list_docshare_api(request):
    try:
        body = get_body(request)
        if isinstance(body, JsonResponse):
            return body
        params = get_params(body, {})
        if isinstance(params, JsonResponse):
            return params
        return success_requests()
    except Exception as e:
        print e
        return err_requests("500", str(e))


def update_docshare_api(request):
    try:
        body = get_body(request)
        if isinstance(body, JsonResponse):
            return body
        params = get_params(body, {})
        if isinstance(params, JsonResponse):
            return params
        return success_requests()
    except Exception as e:
        print e
        return err_requests("500", str(e))


def add_foldershare_api(request):
    try:
        body = get_body(request)
        if isinstance(body, JsonResponse):
            return body
        params = get_params(body, {})
        if isinstance(params, JsonResponse):
            return params
        return success_requests()
    except Exception as e:
        print e
        return err_requests("500", str(e))


def del_foldershare_api(request):
    try:
        body = get_body(request)
        if isinstance(body, JsonResponse):
            return body
        params = get_params(body, {})
        if isinstance(params, JsonResponse):
            return params
        return success_requests()
    except Exception as e:
        print e
        return err_requests("500", str(e))


def info_foldershare_api(request):
    try:
        body = get_body(request)
        if isinstance(body, JsonResponse):
            return body
        params = get_params(body, {})
        if isinstance(params, JsonResponse):
            return params
        return success_requests()
    except Exception as e:
        print e
        return err_requests("500", str(e))


def list_foldershare_api(request):
    try:
        body = get_body(request)
        if isinstance(body, JsonResponse):
            return body
        params = get_params(body, {})
        if isinstance(params, JsonResponse):
            return params
        return success_requests()
    except Exception as e:
        print e
        return err_requests("500", str(e))


def update_foldershare_api(request):
    try:
        body = get_body(request)
        if isinstance(body, JsonResponse):
            return body
        params = get_params(body, {})
        if isinstance(params, JsonResponse):
            return params
        return success_requests()
    except Exception as e:
        print e
        return err_requests("500", str(e))
