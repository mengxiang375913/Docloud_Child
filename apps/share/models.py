# coding=utf-8
from __future__ import unicode_literals

from django.db import models

from apps.organize_manager.models import *
from apps.document.models import Document


class DocumentShare(models.Model):
    document = models.ForeignKey(Document, on_delete=models.CASCADE, verbose_name="文件")
    user_from = models.ForeignKey(CloudAuthUser, related_name="sharefrom", on_delete=models.CASCADE, verbose_name="来自")
    user_to = models.ForeignKey(CloudAuthUser, related_name="sharewith", on_delete=models.CASCADE, verbose_name="推送到")
    share_time = models.DateTimeField(auto_now_add=True, null=True)


class FolderShare(models.Model):
    folder = models.ForeignKey(Document, on_delete=models.CASCADE, verbose_name="文件夹")
    user_from = models.ForeignKey(CloudAuthUser, related_name="foldersharefrom", on_delete=models.CASCADE, verbose_name="来自")
    user_to = models.ForeignKey(CloudAuthUser, related_name="foldersharewith", on_delete=models.CASCADE, verbose_name="推送到")
    share_time = models.DateTimeField(auto_now_add=True, null=True)

# Create your models here.
