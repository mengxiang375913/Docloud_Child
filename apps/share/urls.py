from __future__ import unicode_literals

from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r"^myshare", myshare_view),
    url(r"^sharetome", sharetome_view)
]
