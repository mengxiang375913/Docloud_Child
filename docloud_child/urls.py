"""docloud_child URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
import uuid

from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.views.static import serve

from docloud_child import settings

urlpatterns = [
    # html
    url(r'^admin/', admin.site.urls),
    url(r'^$', include('apps.login.urls')),
    url(r'^media/(?P<path>.*)', serve, {"document_root": settings.MEDIA_ROOT}),
    url(r"^dictionary/", include('apps.dictionary.urls')),
    url(r"^paramsSettings/", include('apps.params_settings.urls')),
    url(r"^orginazation/", include('apps.organize_manager.urls')),
    url(r'^archivesFile/', include('apps.archives_file.urls')),
    url(r"^logs/", include('apps.logs_manager.urls')),
    url(r"^archiveClasses/", include('apps.archive_classes.urls')),
    url(r"^archiveCabinets/", include('apps.archive_cabinets.urls')),
    url(r"^home/", include('apps.index.urls')),
    url(r"^document/", include('apps.document.urls')),
    url(r"^borrow/", include('apps.borrow.urls')),
    url(r"^helper/", include('apps.helper.urls')),
    url(r"^share/", include('apps.share.urls')),
    url(r"^login/", include('apps.login.urls')),
    
    # api
    url(r"^api/logs/", include('apps.logs_manager.api')),
    url(r"^api/orginazation/", include('apps.organize_manager.api')),
    url(r'^api/archiveCabinets/', include('apps.archive_cabinets.api')),
    url(r"^api/paramsSettings/", include('apps.params_settings.api')),
    url(r"^api/dictionary/", include('apps.dictionary.api')),
    url(r"^api/archiveClasses/", include('apps.archive_classes.api')),
    url(r"^api/document/", include('apps.document.api')),
    url(r"^api/share/", include('apps.share.api')),

]

urlpatterns += staticfiles_urlpatterns()
